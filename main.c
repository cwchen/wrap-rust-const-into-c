#include <stdio.h>
#include "const.h"

int main(void)
{
    /* Get `VAR` from Rust. */
    int x = VAR;
    printf("%d\n", x);

    return 0;
}
