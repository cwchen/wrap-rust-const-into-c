# libconst

A tiny C shared library written in Rust and exported to C.

## System Requirements

* Rust
* C compiler such as Clang or GCC
* (Optional) Python with six and cffi modules
* (Optional) Ruby with FFI module

## Usage

Compile the library:

```
$ cargo build --release
```

Run this library with a C main program:

```
$ make
$ LD_LIBRARY_PATH=./target/release ./const
```

Alternatively, run this library with a Python main program:

```
$ python main.py
```

Currently, importing variables into Python is not supported yet.

Alternatively, run this library with a Ruby main program:

```
$ ruby main.rb
```

## Note

Import constant from C to Python is not supported yet.

## Copyright

Copyright (c) 2021 Michelle Chen. Licensed under MIT.
